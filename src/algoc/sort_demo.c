#include <stdio.h>  //printf
#include <stdlib.h> // srand()
#include <time.h>
#include <math.h> //log
// int: 1, float: 2, double: 3
#define DTYPE_MODE 1
#include "algoc/sort.h"
#include "algoc/vector.h"


#define RANDSEED 5

void Benchmark(void (*sortAlgo)(DTYPE *, int), int complexity, int logn) {
    printf("Benchmarking complexity O(n^%d(logn)^%d)\n", complexity, logn);
    int ssize = 4096;
    for (int i = 0; i < 32; i++) {
        DTYPE *A = GenerateRandomVector(ssize);
        clock_t t = clock();
        sortAlgo(A, ssize);
        t = clock() - t;
        float time_elapsed = ((float) t) / CLOCKS_PER_SEC;
        printf("Time for sorting an array with %d elements: %e seconds \n", ssize, 
            time_elapsed);
        float performance = ((float) CLOCKS_PER_SEC) / t;
        for (int j = 0; j < complexity; j++) {
            performance *= ssize;
        }
        for (int j = 0; j < logn; j++) {
            performance *= log(ssize);
        }
        printf("Performance score : %e \n", performance);
        free(A);
        if (time_elapsed > 10) {
            break;
        }
        ssize *= 2;
    }    
}

int main()
{
      // should only be called once
    int const vsize = 30000;
    srand(RANDSEED);
    DTYPE *A = GenerateRandomVector(vsize);
    printf("The generated vector with random seed %d :\n", RANDSEED);
    //PrintVector(A, vsize);
    InsertionSort(A, vsize);
    //printf("The sorted vector :\n");
    //PrintVector(A, vsize);

    srand(RANDSEED);
    DTYPE *B = GenerateRandomVector(vsize);
    QuickSort(B, vsize);
    if (CompareVector(A, B, vsize)) 
        printf("\nInsertion Sort and Quick Sort are consistent\n");
    else
        printf("\nInsertion Sort and Quick Sort are inconsistent\n");
    free(A);
    free(B);


    // Test speed
    printf("Benchmarking Insertion Sort...\n");
    Benchmark(InsertionSort, 2, 0);
    printf("Benchmarking Quick Sort...\n");
    Benchmark(QuickSort, 1, 1);

    return 0;
}




