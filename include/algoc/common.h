#pragma once

// int: 1, float: 2, double: 3
#ifndef DTYPE_MODE
#define DTYPE_MODE 1
#endif

#if DTYPE_MODE == 1
#define DTYPE int
#elif DTYPE_MODE == 2
#define DTYPE float
#elif DTYPE_MODE == 3
#define DTYPE double
#endif

#define TRUE 1
#define FALSE 0