// queue.h
// This is an implementation of linked list queue. Not the ring bufffer queue. 

#pragma once
#include "algoc/common.h"
#include <stdio.h> // printf
#include <stdlib.h> // rand, malloc


#ifdef DEBUG
#define ASSERT assert
#endif

typedef struct FIFOQueueNode {
    void *prev;
    void *next;
    void *item;
} FIFOQueueNode;

FIFOQueueNode *FIFOQueueNodeInit() {
    FIFOQueueNode *node = (FIFOQueueNode *)malloc(sizeof(FIFOQueueNode));
    node->prev = NULL;
    node->next = NULL;
    node->item = NULL;   
    return node;
}

typedef struct FIFOQueue {
    void *first;
    void *last;
    int size;
} FIFOQueue;


FIFOQueue *FIFOQueueInit() {
    FIFOQueue *queue = (FIFOQueue *)malloc(sizeof(FIFOQueue));
    queue->first = NULL;
    queue->last = NULL;
    queue->size = 0;
    return queue;
}

void FIFOQueueRemoveNode(FIFOQueue queue, FIFOQueueNode *node) {
    ASSERT(node != NULL);
    if (node->prev != NULL) {
        node->prev->next = node->next;
    }
    if (node->next != NULL) {
        node->next->prev = node->prev;
    }
    free(node);
    queue->size--;
}

// Directly pop out data instead poping a node,
// the node poped will be removed from the queue and freed
void *FIFOQueuePop(FIFOQueue *queue) {
    FIFOQueueNode *first_node = queue->first;
    void *data = first_node->data;
    FIFOQueueRemoveNode(first_node);
    return data;
}

// Free the entire queue and its nodes
void FIFOQueueFree(FIFOQueue *queue) {
    int size = queue->size;
    for (int i = 0; i < size; i++) {
        FIFOQueueRemoveNode(queue->first);
    }
    free(queue);
}

// insert an allocated node
void FIFOQueueInsertNode(FIFOQueue *queue, void *node) {
    queue->tail->next = node;
    node->prev = queue->tail;
    queue->tail = node;
    queue->size += 1;
}

// item is an allocated address to the concrete data
void FIFOQueueInsertItem(FIFOQueue *queue, void *item) {
    FIFOQueueNode * node = FIFOQueueNodeInit();
    node->data = item;
    FIFOQueueInsertNode(queue, node);
}

DTYPE *GenerateRandomQueue(const int qsize) {
    for (int i = 0; i < qsize; i++) {
      DTYPE *item = malloc(sizeof(DTYPE));
#if DTYPE_MODE == 1
        *item = rand();
#elif DTYPE_MODE == 2 || DTYPE_MODE == 3
        *item = (rand()+0.0) / RAND_MAX;
#endif
    }
    return A;
}