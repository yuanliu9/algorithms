#pragma once
#include "algoc/common.h"

#include "algoc/queue.h"

// operator for greater than
#define OPGT >

// function in header cannot be declared as inline
// so use macro
#define SWAP(A, index_a, index_b) {\
            DTYPE temp = A[index_a];\
            A[index_a] = A[index_b];\
            A[index_b] = temp;\
        }

// Sort a vector
void InsertionSort(DTYPE *A, int A_size) {
    for (int j = 1; j < A_size; j++) {
        int i = j - 1;
        DTYPE key = A[j];
        while (i >= 0 && A[i] OPGT key) {
            A[i + 1] = A[i];
            i--;
        }
        A[i + 1] = key;
    }
}



int QuickSortPartition(DTYPE *A, int A_size) {
    DTYPE key = A[A_size - 1];
    int pivot = 0;
    for (int i = 0; i < A_size - 1; i++) {
        if (key OPGT A[i]) {
            SWAP(A, pivot, i);
            pivot++;
        } 
    }
    SWAP(A, pivot, A_size - 1);
    return pivot;
}


void QuickSort(DTYPE *A, int A_size) {
    if (A_size <= 1) {
        return;
    }
    int r = QuickSortPartition(A, A_size);
    QuickSort(A, r);
    QuickSort(A + r + 1, A_size - r - 1);
}



/*
void IterativeQuickSort(DTYPE *A, int A_size) {
    if (A_size <= 1) {
        return;
    }
    int queue_start_index[A_size];
    int queue_lenth[A_size];
    int queue_size = 1;
    while (queue_size > 0) {
        int start_index = queue_start_index[];
        int r = QuickSortPartition(queue)

    }
    int r = QuickSortPartition(A, A_size);

}
*/








