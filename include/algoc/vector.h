#pragma once
#include "algoc/common.h"
#include <stdio.h> // printf
#include <stdlib.h> // rand, malloc

// TODO: implement test for queue

DTYPE *GenerateRandomVector(const int vsize) {
    DTYPE *A = malloc(sizeof(DTYPE) * vsize);
    for (int i = 0; i < vsize; i++) {
#if DTYPE_MODE == 1
        A[i] = rand();
#elif DTYPE_MODE == 2 || DTYPE_MODE == 3
        A[i] = (rand()+0.0) / RAND_MAX;
#endif
    }
    return A;
}

void PrintVector(DTYPE * const A, int const vsize) {
    for (int i = 0; i < vsize; i++) {
#if DTYPE_MODE == 1
        printf("%d ", A[i]);
#elif DTYPE_MODE == 2 || DTYPE_MODE == 3
        printf("%e ", A[i]);
#endif // DTYPE_MODE
    }
    printf("\n");
}


// MUST ensure both A and B have at least vsize elements
// Compare the first vsize elements of A and B
_Bool CompareVector(DTYPE * const A, DTYPE * const B, int const vsize) {
    _Bool is_consistent = TRUE;
    for (int i = 0; i < vsize && is_consistent; i++) {
        is_consistent &= (A[i] == B[i]);
    }
    return is_consistent;
}
