
CC=gcc

CFLAG=   -pg -g -std=c99 -I ./include

sort_demo: sort_demo.o
	mkdir -p ./build/bin
	$(CC) -o ./build/bin/sort_demo ./build/object/sort_demo.o $(CFLAG)

sort_demo.o: 
	mkdir -p ./build/object
	$(CC) -c -o ./build/object/sort_demo.o ./src/algoc/sort_demo.c $(CFLAG)
